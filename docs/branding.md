## Branding

### How to change colors
Colors can be changed in the points: 
- `Theme` inside geonode admin (you can provide default values inside `fixtures/theme.json`)
- `geoportal/static/less/site_base.less` contains some variables to adjust some base geonode css (**NOTE**: you have to compile `.less` using `gulp`)

### How to change navbar title
Navbar title uses the site name, to change it inside the django admin navigates to `Sites` and change the display name.

### How to auto-upload dynamic image assets
inside `fixture/theme.json` you can define paths for images of slide shows and partners, these however must be uploaded somehow inside django (manually or by collectstatic). You can save assets inside `geoportal/media-fixtures`, all the assets inside this folder will be copied to `{publicDir}/` keeping the same folder-structure. File will be copied by command `collectmedia`, which is run by the `statics` task. For further example check the `main` branch.

### Change print header
Print header must be changed manually by replacing the `docker/geoserver_data/print_header.png` file. When changed you chould re-build the docker container for `geoserver_data`

### Partners - Sponsors
Partners and sponsors are handled by usign a naming convention which is interpreted by the application, example: `03-feamp[sponsor]`.
The first part of the name is used to order between partners/sponsors, order is alphabetical, must end with `-`.
Second part is the actual name, you should keep it simple, unique and without spaces.
Then you should indicate inside square brackets if the entity should be treated as a `sponsor` or a `partner`. 

Refer to branch `main` for examples.
